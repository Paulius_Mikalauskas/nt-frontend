import React from 'react';
import {IndexRoute, Route} from 'react-router';
import {App, Home, NotFound, ObjectDetails, Upload, Edit, Login} from 'containers';
import AuthService from './utils/AuthService';
import {SearchResults} from 'containers';

const auth = new AuthService('yJiHtWrwV6Jyei4D6VTKZEU1VZYfKkCj', 'rex.eu.auth0.com');

const requireAuth = (nextState, replace) => {
  if (!auth.loggedIn()) {
    replace({ pathname: '/login' });
  }
};

const parseAuthHash = (nextState) => {
  if (/access_token|id_token|error/.test(nextState.location.hash)) {
    auth.parseHash(nextState.location.hash);
  }
};

export default() => {
  return (
    <Route path="/" component={App} auth={auth}>
      <IndexRoute component={Home}/>
      <Route path="/admin" title="Naujo objekto forma" component={Upload} onEnter={requireAuth}/>
      <Route path="/edit/:id" component={Edit} onEnter={requireAuth}/>
      <Route path="/objektas/:id" component={ObjectDetails}/>
      <Route path="/objektai" component={SearchResults} />
      <Route path="login" component={Login} onEnter={parseAuthHash} />
      <Route path="*" component={NotFound} status={404}/>
    </Route>
  );
};
