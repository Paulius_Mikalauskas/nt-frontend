import React, {PropTypes} from 'react';
import {Row, Col} from 'react-bootstrap';
import Divider from 'material-ui/Divider';
import Helmet from 'react-helmet';
const styles = require('./ObjectDataDisplay.scss');

const ObjectDataDisplay = (props) => {
  console.log(props);
  const dataDisplay = Object.keys(props.data).map((key, index) => {
    if (props.data[key] !== null && !(props.data[key] instanceof Array)) {
      return (<Col key={index} className={styles.data} xs={4} md={4} lg={4}><span className={styles.key}>{key}: </span>{props.data[key]}</Col>);
    }
  });

  function getRoomWord(value) {
    if (parseInt(value, 10) > 1) {
      return value + ' Kambariai';
    }
    return value + 'Kambarys';
  }

  const getTitle = () => {
    const title = props.data.type + ', ' + props.data.city + ' ' + props.data.street + ' g. ' + props.data.rooms + ' kambarių ' + props.data.type;
    // title = title.replace(new RegExp('null', 'g'), '');
    return title + ' - Mona.lt';
  };

  console.log(dataDisplay);

  const state = props.data.state;
  return (
    <div>
      <Helmet title={getTitle()} defaultTitle="My Default Title" titleAttributes={{
        itemprop: 'name',
        lang: 'lt'
      }} meta={[
        {
          name: 'description',
          content: getTitle()
        }, {
          property: 'og:image',
          content: props.data.images[0].url
        }, {
          property: 'og:type',
          content: 'website'
        }
      ]}/>
      <div className={styles.mainInfo}>
        <div className={styles.state}>{state}</div>
        {props.data.price && <Row>
          <Col xs={4} md={4} lg={4} className={styles.address}>{props.data.price} €</Col>
        </Row>}
        {props.data.rooms && <Row>
          <Col xs={4} md={4} lg={4} className={styles.lowerFont}>{getRoomWord(props.data.rooms)}</Col>
        </Row>}
        <Row>
          <Col xs={4} md={4} lg={4} className={styles.lowerFont}>
          {props.data.street && <span>{props.data.street} g. , </span>}
          {props.data.city && <span>{props.data.city}</span>}</Col>
        </Row>
      </div>
      <Divider/>
      <div className={styles.otherInfo}>
        {props.data.type && <Col className={styles.data} xs={4} md={4} lg={4}><span className={styles.key}>Tipas: </span>{props.data.type}</Col>}
      {props.data.area && <Col className={styles.data} xs={4} md={4} lg={4}><span className={styles.key}>Plotas: </span>{props.data.area} m<sup>2</sup></Col>}
      {props.data.rooms && <Col className={styles.data} xs={4} md={4} lg={4}>{getRoomWord(props.data.rooms)}</Col>}
      {props.data.stories && <Col className={styles.data} xs={4} md={4} lg={4}><span className={styles.key}>Aukštų skaičius: </span>{props.data.stories}</Col>}
      {props.data.story && <Col className={styles.data} xs={4} md={4} lg={4}>{props.data.story} aukštas</Col>}
      {props.data.price && <Col className={styles.data} xs={4} md={4} lg={4}><span className={styles.key}>Kaina: </span>{props.data.price} €</Col>}
      {props.data.catchment && <Col className={styles.data} xs={4} md={4} lg={4}><span className={styles.key}>Mikrorajonas: </span>{props.data.catchment}</Col>}
      {props.data.street && <Col className={styles.data} xs={4} md={4} lg={4}><span className={styles.key}>Gatvė: </span>{props.data.street}</Col>}
      {props.data.city && <Col className={styles.data} xs={4} md={4} lg={4}><span className={styles.key}>Miestas: </span>{props.data.city}</Col>}
      </div>
      <Col className={styles.description} xs={12} md={12} lg={12}>
        <Divider/>
        {props.data.description && <div>
          <div className={styles.key}>Aprašymas: </div>
          <div className={styles.description}>{props.data.description}</div>
        </div>}
      </Col>
    </div>
  );
};

export default ObjectDataDisplay;

ObjectDataDisplay.propTypes = {
  data: PropTypes.object
};
