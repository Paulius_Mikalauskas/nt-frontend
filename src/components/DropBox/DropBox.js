import React, {Component, PropTypes} from 'react';
import Dropzone from 'react-dropzone';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {addImage} from 'redux/modules/form';
import {SelectedImages} from 'components';

@connect(() => ({}), dispatch => bindActionCreators({
  addImage: addImage
}, dispatch))
class DropBox extends Component {

  static propTypes = {
    addImage: PropTypes.func,
    onDropHandler: PropTypes.func
  }

  onDrop(acceptedFiles) {
    this.props.onDropHandler(acceptedFiles);
    // this.getBase64(acceptedFiles[0], (data) => {
    //   this.props.addImage(data);
    // });
  }

  getBase64(file, callback) {
    const reader = new FileReader();
    reader.onload = () => {
      callback(reader.result);
    };
    reader.readAsDataURL(file);
    reader.onerror = (error) => {
      console.log('Error: ', error);
    };
  }

  render() {
    const styles = require('./DropBox.scss');
    return (
      <div>
        <Dropzone ref={(node) => {
          this.dropzone = node;
        }} className={styles.dropZoneStyle} onDrop={this.onDrop.bind(this)}>
          <div className={styles.dropZoneInner}>Imeskite norimas nuotraukas arba paspauskite, kad įkeltumėte čia.</div>
        </Dropzone>
        <SelectedImages/>
      </div>
    );
  }
}

export default DropBox;
