import React, {PropTypes} from 'react';
import {Advertisement} from 'components';
import {Link} from 'react-router';
import Divider from 'material-ui/Divider';

const Category = (props) => {
  const {type, data, title, isLoggedIn, setEditData, anyType, overFlowHidden, ...otherProps} = props;
  const styles = require('./Category.scss');
  let amount = 0;
  const objects = data.map((object, index) => {
    if (object.type === type || props.anyType) {
      amount++;
      return (
        <Link to={`/objektas/${object.id}`} key={object.id} className={styles.item}>
          <div className={styles.columnPadding + ' ' + styles.clickable + ' ' + styles.items}>
            <Advertisement setEditData={setEditData} index={index} objectId={object.id} isLoggedIn={isLoggedIn} data={object}/>
          </div>
        </Link>
      );
    }
  });
  const message = () => {
    if (data.length === 0) {
      return (
        <div>Rezultatų nėra</div>
      );
    }
  };
  return (
    <div>
      {message}
      {amount > 0 && <div className={styles.categoryBlock + ' ' + (overFlowHidden
        ? styles.overFlowHidden
        : styles.overFlowVisible)}>
        <div className={styles.title}>{title}</div><div className={styles.items}>{objects}</div></div>}
        <Divider style={{
          'backgroundColor': 'hsl(0, 0%, 93.3%)',
          'marginTop': '14px'
        }} />
    </div>
  );
};

Category.propTypes = {
  type: PropTypes.any,
  isLoggedIn: PropTypes.bool,
  setEditData: PropTypes.func,
  data: PropTypes.array,
  title: PropTypes.string,
  anyType: PropTypes.bool,
  overFlowHidden: PropTypes.bool
};

Category.defaultProps = {
  anyType: false,
  overFlowHidden: true,
  isLoggedIn: false
};

export default Category;
