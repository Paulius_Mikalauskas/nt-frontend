import React, {PropTypes} from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import {browserHistory} from 'react-router';
const styles = require('./Advertisement.scss');

const Advertisement = (props) => {
  const isPrimary = true;

  function onButtonClicked(event, data) {
    event.preventDefault();
    props.setEditData(data);
    browserHistory.replace('/edit/' + props.objectId);
  }

  return (
    <div className={`${props.containerClass}`}>

      <img className={props.imageClass} src={props.data.images[0].thumbnail}/> {props.isLoggedIn === true && <RaisedButton onClick={(event) => onButtonClicked(event, props.data)} className={styles.editButton} label="Redaguoti" primary={isPrimary}/>}
      <div className={props.infoContainer}>
        <div className={styles.otherInfo}>{props.data.type} {props.data.rooms} k. {props.data.city}</div>
        <div style={{
          'marginBottom': '8px'
        }} className={styles.otherInfo}>{props.data.street} g.</div>
        <div className={styles.otherInfo + ' ' + styles.id}>ID: {props.data.id}</div>
      </div>
    </div>
  );
};

Advertisement.propTypes = {
  data: PropTypes.object,
  isLoggedIn: PropTypes.bool,
  objectId: PropTypes.number,
  setEditData: PropTypes.func,
  containerClass: PropTypes.string,
  imageClass: PropTypes.string,
  infoContainer: PropTypes.string
};

Advertisement.defaultProps = {
  containerClass: styles.adWrapper,
  imageClass: styles.adImage,
  infoContainer: styles.adTextArea
};

export default Advertisement;
