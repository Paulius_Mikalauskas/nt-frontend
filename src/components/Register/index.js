import React, { PropTypes } from 'react';
import TextField from 'material-ui/TextField';

const Register = (props) => {
  return (
      <div>
        <TextField
          hintText="El. paštas"
          fullWidth={Boolean(true)}
          onChange={(event) => props.handleEmailChange(event)}
          value={props.email}
        />
        <TextField
          hintText="Slaptažodis"
          fullWidth={Boolean(true)}
          onChange={(event) => props.handlePasswordChange(event)}
          value={props.password}
        />
        <TextField
          hintText="Slaptažodžio patvirtinimas"
          fullWidth={Boolean(true)}
          onChange={(event) => props.handlePassword2Change(event)}
          value={props.password2}
        />
        {props.isFormValid && <div>Slaptažodžiai nevienodi.</div>}
      </div>
    );
};

Register.propTypes = {
  handleEmailChange: PropTypes.func,
  handlePasswordChange: PropTypes.func,
  handlePasswordChange2: PropTypes.func,
  email: PropTypes.string,
  password: PropTypes.string,
  password2: PropTypes.string,
  isFormValid: PropTypes.bool
};

export default Register;
