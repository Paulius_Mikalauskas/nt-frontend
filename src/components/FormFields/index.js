import React, {Component, PropTypes} from 'react';
import {reduxForm, Field} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {fetchForEditing} from 'redux/modules/objectDetails';

@connect(state => ({initialValues: state.objectForm.formFields}), dispatch => bindActionCreators({fetchForEditing: fetchForEditing}, dispatch))
@reduxForm({form: 'objectForm'})
export default class FormFields extends Component {

  static propTypes = {
    load: PropTypes.func,
    handleSubmit: PropTypes.func,
    initialValues: PropTypes.object,
    fetchForEditing: PropTypes.func,
    initialize: PropTypes.func
  }

  render() {
    const styles = require('./Form.scss');
    const fieldsData = [
      {name: 'type', label: 'Tipas'},
      {name: 'city', label: 'Miestas'},
      {name: 'catchment', label: 'Mikrorajonas'},
      {name: 'street', label: 'Gatvė'},
      {name: 'area', label: 'Plotas'},
      {name: 'story', label: 'Aukštas'},
      {name: 'stories', label: 'Aukštų skaičius'},
      {name: 'rooms', label: 'Kamabarių skaičius'},
      {name: 'price', label: 'Kaina'},
    ];

    const fields = fieldsData.map((field) => {
      return (
        <div className={styles.textFieldContainer} key={field.label}>
          <span>
            <Field fullWidth={Boolean(true)} name={field.name} component={TextField} floatingLabelText={field.label}/>
            <span className={styles.myspace}></span>
          </span>
        </div>
      );
    });
    return (
      <form onSubmit={() => this.handleSubmit()}>
        {fields}
        <div className={styles.textFieldContainer}>
          <span>
            <Field multiLine={Boolean(true)} fullWidth={Boolean(true)} name="description" component={TextField} floatingLabelText="Aprašymas"/>
            <span className={styles.myspace}></span>
          </span>
        </div>
      </form>
    );
  }
}
