import React, { PropTypes } from 'react';
import TextField from 'material-ui/TextField';

const Login = (props) => {
  return (
      <div>
        <TextField
          hintText="El. paštas"
          fullWidth={Boolean(true)}
          onChange={(event) => props.handleEmailChange(event)}
          value={props.email}
        />
        <TextField
          hintText="Slaptažodis"
          fullWidth={Boolean(true)}
          onChange={(event) => props.handlePasswordChange(event)}
          value={props.password}
        />
      </div>
    );
};

Login.propTypes = {
  handleEmailChange: PropTypes.func,
  handlePasswordChange: PropTypes.func,
  email: PropTypes.string,
  password: PropTypes.string,
};

export default Login;
