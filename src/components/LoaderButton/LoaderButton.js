import React, {PropTypes} from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import {grey50} from 'material-ui/styles/colors';

const LoaderButton = (props) => {
  const styles = require('./LoaderButton.scss');
  function onButtonClicked() {
    props.myEvent();
  }
  return (
    <span className={styles.loaderBtn}>
      {props.isLoading ? (
       <CircularProgress />
     ) : (
       <RaisedButton labelColor={grey50} backgroundColor={props.color} onClick={() => onButtonClicked()} label={props.name} />
     )}
     <span className={(props.isError ? styles.error : styles.success)}>{props.message}</span>
   </span>
  );
};

LoaderButton.propTypes = {
  isLoading: PropTypes.bool,
  message: PropTypes.string,
  isError: PropTypes.bool,
  name: PropTypes.string,
  color: PropTypes.string
};


export default LoaderButton;
