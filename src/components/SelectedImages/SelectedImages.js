import React, {Component, PropTypes} from 'react';
import {Row, Col} from 'react-bootstrap';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import {grey50} from 'material-ui/styles/colors';

class SelectedImages extends Component {

  static propTypes = {
    images: PropTypes.array,
    onRemove: PropTypes.func
  };

  static defaultProps = {
    images: []
  }

  render() {
    const styles = require('./SelectedImages.scss');
    const images = this.props.images.map((image, index) => {
      return (
        <Col xs={3} md={3} lg={3} key={index} className={styles.mainWrapper}>
            <img className={styles.img} src={image.preview}/>
            <CloseIcon onClick={() => this.props.onRemove(image)} className={styles.exitIcon} color={grey50} />
        </Col>
      );
    });
    return (
      <Row>
        {images}
      </Row>
    );
  }
}

export default SelectedImages;
