import React, {Component} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import {green500, green50} from 'material-ui/styles/colors';
import TextField from 'material-ui/TextField';

export default class ContactCard extends Component {

  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  render() {
    const styles = require('./ContactCard.scss');
    const actions = [
      <FlatButton
        label="Atšaukti"
        primary={Boolean(true)}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label="Siųsti"
        primary={Boolean(true)}
        keyboardFocused={Boolean(true)}
        onTouchTap={this.handleClose}
      />,
    ];

    return (
      <div>
        <div>
          <RaisedButton backgroundColor={green500} labelColor={green50} className={styles.cardBtn} label="Susisiekite su mumis" onTouchTap={this.handleOpen} />
        </div>
        <Dialog
          title="Susisiekite su mumis"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          contentClassName={styles.contactCardStyle}>
          Užpildykite formą esančią apačioje.<br/>
          <TextField fullWidth={Boolean(true)} hintText="Vardas"/><br/>
          <TextField fullWidth={Boolean(true)} hintText="El. paštas"/><br/>
          <TextField multiLine={Boolean(true)} rows={2} fullWidth={Boolean(true)} hintText="Jūsų žinutė"/><br/>
        </Dialog>
      </div>
    );
  }
}
