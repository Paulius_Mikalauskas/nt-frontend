import React, {Component, PropTypes} from 'react';
import {List, ListItem} from 'material-ui/List';
import ContentInbox from 'material-ui/svg-icons/action/home';
import KeyBoardArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right';
import EditIcon from 'material-ui/svg-icons/image/edit';
import ExitIcon from 'material-ui/svg-icons/action/exit-to-app';
import Divider from 'material-ui/Divider';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {fetchById} from 'redux/modules/advertisement';
import {search} from 'redux/modules/searchForm';
import {browserHistory} from 'react-router';
import {restartForm} from 'redux/modules/form';

@connect((state) => ({isLoggedIn: state.login.isLoggedIn}), dispatch => bindActionCreators({
  fetchById: fetchById,
  search: search,
  restartForm: restartForm
}, dispatch))
export default class SideMenu extends Component {
  static propTypes = {
    isToggled: PropTypes.bool,
    isLoggedIn: PropTypes.bool,
    logout: PropTypes.func,
    setStatus: PropTypes.func,
    search: PropTypes.func,
    restartForm: PropTypes.func
  };

  logout() {
    this.props.logout();
    this.props.setStatus(false);
  }

  search(query) {
    browserHistory.replace('/objektai');
    this.props.search({type: query});
  }

  render() {
    const listItem = {
      'fontSize': '14px',
      'fontFamily': 'Roboto, Arial, sans-serif',
      'height': '40px',
      'lineHeight': '8px'
    };
    const searchData = [
      {name: 'Namai', query: 'namas'},
      {name: 'Butai', query: 'butas'},
      {name: 'Kotedžai', query: 'kotedžas'},
      {name: 'Sodai', query: 'sodas'},
      {name: 'Patalpos', query: 'patalpa'},
      {name: 'Sklypai', query: 'sklypas'}
    ];

    const searchItems = searchData.map((item, index) => {
      return (
        <ListItem key={index} innerDivStyle={listItem} onClick={() => this.search(item.query)} primaryText={item.name} leftIcon={< KeyBoardArrowRight viewBox="-4 4 24 24" />}/>
      );
    });
    const styles = require('./SideMenu.scss');
    return (
      <div className={styles.list + ' ' + (this.props.isToggled
        ? styles.sideMenuOpen
        : styles.sideMenuClosed)}>
        <List>
          <Link to={'/'}>
              <ListItem innerDivStyle={listItem} primaryText="Pradžia" leftIcon={< ContentInbox viewBox="-4 4 24 24" />}/>
          </Link>
          {searchItems}
          <Divider className={styles.dividerStyle}/>
          {this.props.isLoggedIn === true &&
            <div>
            <Link to={'/admin'} onClick={() => this.props.restartForm()}> <ListItem innerDivStyle={listItem} primaryText="Sukurti objektą" leftIcon={<EditIcon viewBox="-4 4 24 24" />}/></Link>
            <ListItem innerDivStyle={listItem} onClick={() => this.logout()} primaryText="Atsijungti" leftIcon={<ExitIcon viewBox="-4 4 24 24" />}/>
            </div>
          }
        </List>
      </div>
    );
  }
}

SideMenu.defaultProps = {
  isToggled: true
};
