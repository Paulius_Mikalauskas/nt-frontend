import React, {PropTypes} from 'react';
import Lightbox from 'react-images';

const ImageViewer = (props) => {
  const styles = require('./ImageViewer.scss');
  const images = [];
  props.images.map((img) => {
    images.push({src: img.url});
  });
  return (
    <div className={styles.wrapper}>
      <div className={styles.imageWrapper}>
        <img onClick={() => props.open()} className={styles.displayImage} src={props.images[props.images.length - 1].url}/>
      </div>
      <div className={styles.imageInfo}>Nuotraukos ({props.images.length})</div>
      <Lightbox
       images={images}
       isOpen={props.isOpen}
       onClickPrev={() => props.decrement()}
       onClickNext={() => props.increment()}
       onClose={() => props.close()}
       currentImage={props.index}
     />
    </div>
  );
};

ImageViewer.propTypes = {
  index: PropTypes.number,
  images: PropTypes.array,
  open: PropTypes.func,
  close: PropTypes.func,
  increment: PropTypes.func,
  decrement: PropTypes.func,
  isOpen: PropTypes.bool
};

export default ImageViewer;
