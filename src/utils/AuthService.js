import {EventEmitter} from 'events';
import {isTokenExpired} from './jwtHelper';
import {browserHistory} from 'react-router';
import auth0 from 'auth0-js';
import cookie from 'react-cookie';
export default class AuthService extends EventEmitter {
  constructor() {
    super();
    // Configure Auth0
    this.auth0 = new auth0.WebAuth({clientID: 'V3W2QsiAaPLuC991R7oxfyUsrYdWkU41', domain: 'rex.eu.auth0.com', responseType: 'token id_token', redirectUri: 'http://localhost:3000/admin'});
    // this.login = this.login.bind(this);
    // this.signup = this.signup.bind(this);
  }

  login(username, password) {
    this.auth0.client.login({
      realm: 'Username-Password-Authentication',
      username,
      password
    }, (err, authResult) => {
      if (err) {
        alert('Error: ' + err.description);
        return;
      }
      if (authResult && authResult.idToken && authResult.accessToken) {
        this.setToken(authResult.accessToken, authResult.idToken);
        browserHistory.replace('/admin');
      }
    });
  }

  parseHash(hash) {
    this.auth0.parseHash({
      hash
    }, (err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setToken(authResult.accessToken, authResult.idToken);
        browserHistory.replace('/home');
        this.auth0.client.userInfo(authResult.accessToken, (error, profile) => {
          if (error) {
            console.log('Error loading the Profile', error);
          } else {
            this.setProfile(profile);
          }
        });
      } else if (authResult && authResult.error) {
        alert('Error: ' + authResult.error);
      }
    });
  }

  loggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken();
    console.log(token);
    console.log(isTokenExpired(token));
    return !!token && !isTokenExpired(token);
  }

  setToken(accessToken, idToken) {
    // Saves user access token and ID token into local storage
    cookie.save('access_token', accessToken, {path: '/'});
    cookie.save('id_token', idToken, {path: '/'});
  }
  // localStorage.setItem('access_token', accessToken);
  // localStorage.setItem('id_token', idToken);

  setProfile(profile) {
    cookie.save('profile', JSON.stringify(profile), {path: '/'});
    // Saves profile data to localStorage
    // localStorage.setItem('profile', JSON.stringify(profile));
    // Triggers profile_updated event to update the UI
    this.emit('profile_updated', profile);
  }

  getProfile() {
    // Retrieves the profile data from localStorage
    const profile = localStorage.getItem('profile');
    return profile
      ? JSON.parse(profile)
      : {};
  }

  getToken() {
    return cookie.load('id_token');
    // Retrieves the user token from localStorage
    // return localStorage.getItem('id_token');
  }

  logout() {
    cookie.remove('id_token', {path: '/'});
    cookie.remove('profile', {path: '/'});
    // Clear user token and profile data from localStorage
    // localStorage.removeItem('id_token');
    // localStorage.removeItem('profile');
  }
}
