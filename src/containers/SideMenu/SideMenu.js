import React, {Component, PropTypes} from 'react';
import {List, ListItem} from 'material-ui/List';
import ContentInbox from 'material-ui/svg-icons/action/home';
import KeyBoardArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right';
import EditIcon from 'material-ui/svg-icons/image/edit';
import ExitIcon from 'material-ui/svg-icons/action/exit-to-app';
import Divider from 'material-ui/Divider';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {fetchById} from 'redux/modules/advertisement';
@connect((state) => ({isToggled: state.sidemenu.isToggled, isLoggedIn: state.auth.isLoggedIn}), dispatch => bindActionCreators({
  fetchById: fetchById
}, dispatch))
export default class SideMenu extends Component {
  static propTypes = {
    isToggled: PropTypes.bool,
    isLoggedIn: PropTypes.bool,
    logout: PropTypes.func,
    setStatus: PropTypes.func,
    fetchById: PropTypes.func
  };

  logout() {
    this.props.logout();
    this.props.setStatus(false);
  }

  render() {
    const listItem = {
      'fontSize': '14px',
      'fontFamily': 'Roboto, Arial, sans-serif'
    };

    console.log(this.props);
    const styles = require('./SideMenu.scss');
    return (
      <div className={styles.list + ' ' + (this.props.isToggled
        ? styles.sideMenuOpen
        : styles.sideMenuClosed)}>
        <List>
          <Link to={'/'}>
            <ListItem innerDivStyle={listItem} primaryText="Home" leftIcon={< ContentInbox />}/>
          </Link>
          <ListItem innerDivStyle={listItem} onClick={() => this.props.fetchById('namas')} primaryText="Namai" leftIcon={< KeyBoardArrowRight />}/>
          <ListItem innerDivStyle={listItem} onClick={() => this.props.fetchById('butas')} primaryText="Butai" leftIcon={< KeyBoardArrowRight />}/>
          <ListItem innerDivStyle={listItem} primaryText="Kotedžai" leftIcon={< KeyBoardArrowRight />}/>
          <ListItem innerDivStyle={listItem} primaryText="Vilos" leftIcon={< KeyBoardArrowRight />}/>
          <Divider className={styles.dividerStyle}/>
          {this.props.isLoggedIn === true &&
            <div>
            <Link to={'/admin'}> <ListItem innerDivStyle={listItem} primaryText="Sukurti objektą" leftIcon={<EditIcon />}/></Link>
            <ListItem innerDivStyle={listItem} onClick={() => this.logout()} primaryText="Atsijungti" leftIcon={<ExitIcon />}/>
            </div>
          }
        </List>
      </div>
    );
  }
}

SideMenu.defaultProps = {
  isToggled: true
};
