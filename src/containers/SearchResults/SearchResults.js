import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {setEditData} from 'redux/modules/form';
import {Category} from 'components';
import {SearchForm} from 'containers';

@connect((state) => ({results: state.searchForm.results, isLoggedIn: state.login.isLoggedIn}), dispatch => bindActionCreators({
  setEditData: setEditData
}, dispatch))
export default class SearchResults extends Component {

  static propTypes = {
    results: PropTypes.array,
    isLoggedIn: PropTypes.bool,
    setEditData: PropTypes.func
  }

  render() {
    const buttonStyle = {
      'border': '1px solid #4CAF50',
      'marginLeft': '5px',
      'backgroundColor': '#4CAF50',
      'color': 'white',
      'paddingBottom': '1px',
      'height': '35px'
    };

    const popbtnStyle = {
      'border': '1px solid #C8E6C9',
      'paddingLeft': '8px',
    };
    const styles = require('./SearchResults.scss');
    return (
      <div>
        <SearchForm
          btnStyle={popbtnStyle}
          buttonStyle={buttonStyle}
          containerClass={styles.popover}
        />
        <Category
          title="Paieškos rezultatai"
          overFlowHidden={Boolean(false)}
          anyType={Boolean(true)}
          setEditData={this.props.setEditData}
          data={this.props.results}
          isLoggedIn={this.props.isLoggedIn}
         />
      </div>
    );
  }
}
