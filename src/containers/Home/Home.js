import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {isFetched, fetch} from 'redux/modules/advertisement';
import {asyncConnect} from 'redux-async-connect';
import {bindActionCreators} from 'redux';
import Helmet from 'react-helmet';
import {setIndex} from 'redux/modules/imageViewer';
import {setEditData} from 'redux/modules/form';
import {Category} from 'components';
import {SearchForm} from 'containers';
const styles = require('./Home.scss');

@asyncConnect([
  {
    deferred: true,
    promise: ({
      store: {
        dispatch,
        getState
      }
    }) => {
      if (!isFetched(getState())) {
        return dispatch(fetch());
      }
    }
  }
])
@connect((state) => ({advertisement: state.advertisement, isLoggedIn: state.login.isLoggedIn}), dispatch => bindActionCreators({
  setIndex: setIndex,
  setEditData: setEditData
}, dispatch))
export default class Home extends Component {
  static propTypes = {
    advertisement: PropTypes.object,
    setIndex: PropTypes.func,
    isLoggedIn: PropTypes.bool,
    setEditData: PropTypes.func
  }
  render() {
    const buttonStyle = {
      'border': '1px solid #4CAF50',
      'marginLeft': '5px',
      'backgroundColor': '#4CAF50',
      'color': 'white',
      'paddingBottom': '1px',
      'height': '35px'
    };

    const popbtnStyle = {
      'border': '1px solid #C8E6C9',
      'paddingLeft': '8px',
    };
    return (
      <div className={styles.bgColor}>
        <Helmet title="Nekilnojamo turto skelbimai - Mona.lt" defaultTitle="My Default Title" titleAttributes={{
          itemprop: 'name',
          lang: 'lt'
        }} meta={[
          {
            name: 'description',
            content: 'Helmet application'
          }, {
            property: 'og:image',
            content: 'http://www.returta.lt/images/sv1/images/dgvdb_2_.jpg'
          }, {
            property: 'og:type',
            content: 'website'
          }
        ]}/>
        <div>
          <SearchForm btnStyle={popbtnStyle} buttonStyle={buttonStyle} containerClass={styles.popover}/>
          <Category title="Butai" setEditData={this.props.setEditData} type="Butas" data={this.props.advertisement.ads} isLoggedIn={this.props.isLoggedIn}/>
          <Category title="Namai" setEditData={this.props.setEditData} type="Namas" data={this.props.advertisement.ads} isLoggedIn={this.props.isLoggedIn}/>
          <Category title="Kotedžai" setEditData={this.props.setEditData} type="Kotedžas" data={this.props.advertisement.ads} isLoggedIn={this.props.isLoggedIn}/>
          <Category title="Sodai" setEditData={this.props.setEditData} type="Sodas" data={this.props.advertisement.ads} isLoggedIn={this.props.isLoggedIn}/>
          <Category title="Patalpos" setEditData={this.props.setEditData} type="Patalpa" data={this.props.advertisement.ads} isLoggedIn={this.props.isLoggedIn}/>
          <Category title="Sklypai" setEditData={this.props.setEditData} type="Sklypas" data={this.props.advertisement.ads} isLoggedIn={this.props.isLoggedIn}/>
        </div>
      </div>
    );
  }
}
