import React, {Component, PropTypes} from 'react';
import Divider from 'material-ui/Divider';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {upload} from 'redux/modules/upload';
import {DropBox} from 'components';
import {FormFields, SelectedImages} from 'components';
import {green500} from 'material-ui/styles/colors';
import {LoaderButton} from 'components';

@connect((state) => ({
  form: state.form,
  isFetching: state.uploadReducer.isLoading,
  isError: state.uploadReducer.isFailed,
  message: state.uploadReducer.message,
}), dispatch => bindActionCreators({
  upload: upload,
}, dispatch))
export default class Upload extends Component {

  static propTypes = {
    upload: PropTypes.func,
    form: PropTypes.object,
    isFetching: PropTypes.bool,
    isError: PropTypes.bool,
    message: PropTypes.string,
  }

  constructor(props) {
    super(props);
    this.state = {
      images: []
    };
    this.handleOnDrop = this.handleOnDrop.bind(this);
    this.handleRemoveImage = this.handleRemoveImage.bind(this);
  }

  handleOnDrop(image) {
    this.setState({images: this.state.images.concat(image)});
  }

  handleRemoveImage(image) {
    const index = this.state.images.indexOf(image);
    if (index !== -1) {
      this.setState({images: [...this.state.images.slice(0, index), ...this.state.images.slice(index + 1)]});
    }
  }

  upload = () => {
    this.props.upload(this.state.images, this.props.form.objectForm.values);
  }

  render() {
    const styles = require('./index.scss');
    return (
      <div className={styles.mycontainer}>
        <div className={styles.borderStyle}>
          <div className={styles.mainWrapper}>
            <Divider/>
            <FormFields/>
            <h4>Objekto nuotraukų ikėlimas.</h4>
            <div className={styles.dropBoxWrapper}>
              <DropBox onDropHandler={this.handleOnDrop}/>
              <SelectedImages images={this.state.images} onRemove={this.handleRemoveImage}/>
            </div>
            <Divider/>
            <LoaderButton color={green500} myEvent={this.upload} message={this.props.message} name="Įkelti" isLoading={this.props.isFetching} isError={this.props.isError}/>
          </div>
        </div>
      </div>
    );
  }
}
