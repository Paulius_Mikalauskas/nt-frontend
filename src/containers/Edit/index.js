import React, {Component, PropTypes} from 'react';
import Divider from 'material-ui/Divider';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {update, addImage, removeImage, removeAdvert} from 'redux/modules/update';
import {DropBox} from 'components';
import {FormFields, SelectedImages} from 'components';
import {green500} from 'material-ui/styles/colors';
import {red500} from 'material-ui/styles/colors';
import {LoaderButton} from 'components';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

@connect((state) => ({
  form: state.form,
  form2: state.objectForm,
  isUpdateLoading: state.updateReducer.isUpdateLoading,
  isUpdateFailed: state.updateReducer.isUpdateFailed,
  isRemoveLoading: state.updateReducer.isRemoveLoading,
  isRemoveFailed: state.updateReducer.isRemoveFailed,
  updateMessage: state.updateReducer.message,
  removeMessage: state.updateReducer.removeMsg,
}), dispatch => bindActionCreators({
  update: update,
  addImage: addImage,
  removeImage: removeImage,
  removeAdvert: removeAdvert
}, dispatch))
export default class Edit extends Component {

  static propTypes = {
    removeAdvert: PropTypes.func,
    addImage: PropTypes.func,
    update: PropTypes.func,
    remove: PropTypes.func,
    removeImage: PropTypes.func,
    form: PropTypes.object,
    form2: PropTypes.object,
    isUpdateLoading: PropTypes.bool,
    isUpdateFailed: PropTypes.bool,
    isRemoveLoading: PropTypes.bool,
    isRemoveFailed: PropTypes.bool,
    updateMessage: PropTypes.string,
    removeMessage: PropTypes.string,
    params: PropTypes.object,
  }

  constructor(props) {
    super(props);
    const data = props.form2.selectedImages;
    let images = [];
    if (data && data !== undefined) {
      images = data.map((image) => {
        return {id: image.id, preview: image.url};
      });
    }
    this.state = {
      images: images,
      isDialogOpen: false,
    };
    this.handleOnDrop = this.handleOnDrop.bind(this);
    this.handleRemoveImage = this.handleRemoveImage.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleOpen = () => {
    this.setState({isDialogOpen: true});
  };

  handleClose = () => {
    this.setState({isDialogOpen: false});
  };

  handleDelete = () => {
    this.setState({isDialogOpen: false});
    this.props.removeAdvert(this.props.params.id);
  }

  handleOnDrop(image) {
    console.log(image);
    this.props.addImage(image[0], this.props.params.id, (data) => {
      this.setState({images: this.state.images.concat([{...image[0], id: data}])}, () => {
        console.log(this.state.images);
      });
    });
  }

  handleRemoveImage(image) {
    const index = this.state.images.indexOf(image);
    if (index !== -1) {
      this.props.removeImage(image.id, () => {
        this.setState({images: [...this.state.images.slice(0, index), ...this.state.images.slice(index + 1)]});
      });
    }
  }

  handleUpdate = () => {
    this.props.update(this.props.form.objectForm.values, this.props.params.id);
  }

  render() {
    const styles = require('./index.scss');
    const actions = [ < FlatButton label = "Atšaukti" primary = {
        Boolean(true)
      }
      onTouchTap = {
        this.handleClose
      } />, < FlatButton label = "Ištrinti" primary = {
        Boolean(true)
      }
      onTouchTap = {
        this.handleDelete
      } />
    ];
    return (
      <div className={styles.mycontainer}>
        <div className={styles.borderStyle}>
          <div className={styles.mainWrapper}>
            <Dialog actions={actions} modal={false} open={this.state.isDialogOpen} onRequestClose={this.handleClose}>
              Ar tikrai norite ištrinti?
            </Dialog>
            <Divider/>
            <FormFields/>
            <h4>Objekto nuotraukų ikėlimas.</h4>
            <div className={styles.dropBoxWrapper}>
              <DropBox onDropHandler={this.handleOnDrop}/>
              <SelectedImages images={this.state.images} onRemove={this.handleRemoveImage}/>
            </div>
            <Divider/>
            <div>
              <LoaderButton
                color={green500}
                className={styles.updateBtn}
                myEvent={this.handleUpdate}
                message={this.props.updateMessage}
                name="Atnaujinti"
                isLoading={this.props.isUpdateLoading}
                isError={this.props.isUpdateFailed}/>

              <LoaderButton
                color={red500}
                className={styles.removeBtn}
                myEvent={this.handleOpen}
                message={this.props.removeMessage}
                name="Ištrinti objektą"
                isLoading={this.props.isRemoveLoading}
                isError={this.props.isRemoveFailed}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
