import React, {Component, PropTypes} from 'react';
import {Row, Col, Grid} from 'react-bootstrap';
import {asyncConnect} from 'redux-async-connect';
import {connect} from 'react-redux';
import {fetch} from 'redux/modules/objectDetails';
import * as viewerActions from 'redux/modules/imageViewer';
import {ImageViewer, ObjectDataDisplay, ContactCard} from 'components';
@asyncConnect([
  {
    deferred: true,
    promise: ({store: {
        dispatch
      }, params}) => {
      return dispatch(fetch(params.id));
    }
  }
])
@connect(state => ({details: state.objectDetails.details, viewerIndex: state.imageViewer.index, isOpen: state.imageViewer.isOpen}), {
  ...viewerActions,
  fetch
})
class ObjectDetails extends Component {

  static propTypes = {
    details: PropTypes.object,
    setIndex: PropTypes.func,
    viewerIndex: PropTypes.number,
    params: PropTypes.object,
    fetch: PropTypes.func,
    open: PropTypes.func,
    close: PropTypes.func,
    increment: PropTypes.func,
    decrement: PropTypes.func,
    isOpen: PropTypes.bool
  };

  componentDidMount() {
    if (this.props.details === null) {
      this.props.fetch(this.props.params.id);
    }
  }

  clicked(event) {
    event.persist();
    this.props.setIndex(this.props.details.images.indexOf(event.target.src));
  }

  render() {
    const styles = require('./ObjectDetails.scss');
    return (
      <Grid className={styles.container}>
        <Row className={styles.rowPadding}>
          <Col xs={12} md={12} lg={12} className={styles.firstCol}>
            <div className={styles.imageWrapper}>
              <img className={styles.displayimage} src={this.props.details.images[0].url}/>
              <ImageViewer increment={this.props.increment} decrement={this.props.decrement} open={this.props.open} close={this.props.close} images={this.props.details.images} index={this.props.viewerIndex} isOpen={this.props.isOpen}/>
              <ContactCard />
            </div>
          </Col>
          <Col className={styles.fieldWrapper} xs={12} md={12} lg={12}>
            <ObjectDataDisplay data={this.props.details} />
            <br />
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default ObjectDetails;
