import React, {Component, PropTypes} from 'react';
import BasePopover from '../BasePopover/BasePopover';
import TextField from 'material-ui/TextField';
import Room from 'material-ui/svg-icons/action/room';

export default class RoomPopover extends Component {

  static propTypes = {
    start: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    end: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    setStart: PropTypes.func,
    setEnd: PropTypes.func
  }

  getRoomName() {
    if (this.props.start === '' && this.props.end === '') {
      return 'Kambarių skaičius';
    }
    if (this.props.start === '' && this.props.end !== '') {
      return 'Iki ' + this.props.end + ' kambarių';
    }
    if (this.props.start !== '' && this.props.end === '') {
      return 'Nuo ' + this.props.start + ' kambarių';
    }
    return this.props.start + ' - ' + this.props.end + ' kambarių';
  }

  getErrorText() {
    if (parseInt(this.props.start, 10) > parseInt(this.props.end, 10)) {
      return 'Kambarių skaičius negali būti didesnis už galutinę ribą';
    }
    return '';
  }

  render() {
    const iconStyle = {
      'marginTop': '6px',
      'float': 'left',
      'marginLeft': '12px'
    };
    const styles = require('./Room.scss');
    const {setStart, start, setEnd, end, ...other} = this.props;
    return (
      <BasePopover {...other} icon={<Room style={iconStyle} />} name={this.getRoomName()}>
        <TextField errorText={this.getErrorText()} onChange={(object, value) => setStart(value)} value={start} type="number" className={styles.fieldWidth} floatingLabelText="Kambarių sk. nuo" floatingLabelFixed={Boolean(true)}/>
        <span className={styles.gap}> </span>
        <TextField className={styles.fieldWidth} onChange={(object, value) => setEnd(value)} value={end} type="number" floatingLabelText="Kambarių sk. iki" floatingLabelFixed={Boolean(true)}/>
      </BasePopover>
    );
  }
}
