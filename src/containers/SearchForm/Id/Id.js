import React, {Component, PropTypes} from 'react';
import BasePopover from '../BasePopover/BasePopover';
import TextField from 'material-ui/TextField';
import VpnKey from 'material-ui/svg-icons/communication/vpn-key';

export default class IdPopover extends Component {
  static propTypes = {
    id: PropTypes.node,
    setId: PropTypes.func
  }

  getIdName() {
    if (this.props.id === '') {
      return 'Id';
    }
    return this.props.id;
  }
  render() {
    const {setId, id, ...other} = this.props;
    const iconStyle = {
      'marginTop': '5px',
      'float': 'left',
      'marginLeft': '12px'
    };
    return (
      <BasePopover {...other} name={this.getIdName()} icon={<VpnKey style={iconStyle} />}>
        <TextField onChange={(object, value) => setId(value)} value={id} type="number" floatingLabelText="Id numeris" floatingLabelFixed={Boolean(true)}/>
      </BasePopover>
    );
  }
}
