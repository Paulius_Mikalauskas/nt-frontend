import React, {Component, PropTypes} from 'react';
import BasePopover from '../BasePopover/BasePopover';
import TextField from 'material-ui/TextField';
import AttachMoney from 'material-ui/svg-icons/editor/attach-money';

export default class PricePopover extends Component {

  static propTypes = {
    start: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    end: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    setStart: PropTypes.func,
    setEnd: PropTypes.func,
    containerClass: PropTypes.string
  }

  getPriceName() {
    if (this.props.start === '' && this.props.end === '') {
      return 'Kaina';
    }
    if (this.props.start === '' && this.props.end !== '') {
      return '0€ - ' + this.props.end + '€';
    }
    if (this.props.start !== '' && this.props.end === '') {
      return this.props.start + '€ +';
    }
    return this.props.start + '€ - ' + this.props.end + '€';
  }

  getErrorText() {
    if (parseInt(this.props.start, 10) > parseInt(this.props.end, 10)) {
      return 'Kaina negali būti didesnė už galutinę ribą';
    }
    return '';
  }

  render() {
    const iconStyle = {
      'marginTop': '6px',
      'float': 'left',
    };
    const styles = require('./Price.scss');
    const {setStart, start, setEnd, end, ...other} = this.props;
    return (
      <BasePopover {...other} icon={<AttachMoney style={iconStyle} />} name={this.getPriceName()}>
        <TextField errorText={this.getErrorText()} onChange={(object, value) => setStart(value)} value={start} type="number" className={styles.fieldWidth} floatingLabelText="Kaina nuo" floatingLabelFixed={Boolean(true)}/>
        <span className={styles.gap}> </span>
        <TextField className={styles.fieldWidth} onChange={(object, value) => setEnd(value)} value={end} type="number" floatingLabelText="Kaina iki" floatingLabelFixed={Boolean(true)}/>
      </BasePopover>
    );
  }
}
