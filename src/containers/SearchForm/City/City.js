import React, {Component, PropTypes} from 'react';
import AutoComplete from 'material-ui/AutoComplete';
import debounce from 'lodash/debounce';
import BasePopover from '../BasePopover/BasePopover';
import LocationCity from 'material-ui/svg-icons/social/location-city';

export default class CityPopover extends Component {

  static propTypes = {
    autoComplete: PropTypes.func,
    predictions: PropTypes.array,
    setCityName: PropTypes.func,
    cityName: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.autoComplete = debounce(this.autoComplete, 300);
    this.state = {
      isOpen: false
    };
  }

  setQuery(city) {
    return 'input=' + city + '&types=geocode&language=lt';
  }

  getCityName() {
    if (this.props.cityName === '') {
      return 'Miestas';
    }
    return this.props.cityName;
  }

  handleUpdateInput = (value) => {
    this.props.setCityName(value);
    this.autoComplete(value);
  };

  autoComplete(value) {
    this.props.autoComplete(this.setQuery(value));
  }

  render() {
    const iconStyle = {
      'marginTop': '6px',
      'float': 'left',
      'marginLeft': '12px'
    };
    const {setCityName, predictions, cityName, ...other} = this.props;
    return (
      <BasePopover {...other} icon={<LocationCity style={iconStyle} />} isOpen={this.state.isOpen} name={this.getCityName()}>
        <AutoComplete
          searchText={cityName}
          onClose={() => this.setState({isOpen: false})}
          onNewRequest={(value) => setCityName(value)}
          hintText="Miestas"
          dataSource={predictions}
          onUpdateInput={this.handleUpdateInput}
          filter={() => true}/>
      </BasePopover>
    );
  }
}
