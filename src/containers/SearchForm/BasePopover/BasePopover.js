import React, {Component, PropTypes} from 'react';
import FlatButton from 'material-ui/FlatButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
const styles = require('./BasePopover.scss');
export default class BasePopover extends Component {

  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array
    ]),
    name: PropTypes.string,
    isOpen: PropTypes.bool,
    icon: PropTypes.element,
    isFullWidth: PropTypes.bool,
    containerClass: PropTypes.string,
    btnStyle: PropTypes.object
  }

  static defaultProps = {
    isOpen: false,
    isFullWidth: true,
    containerClass: styles.container,
    btnStyle: {
      'paddingLeft': '8px',
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      open: this.props.isOpen,
    };
  }

  handleTouchTap = (event) => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    const labelStyle = {
      'textTransform': 'capitalize',
      'float': 'left',
      'fontWeight': 'normal',
      'marginLeft': '2px'
    };

    return (
      <div className={this.props.containerClass}>
      <FlatButton
        onTouchTap={this.handleTouchTap}
        label={this.props.name}
        labelStyle={labelStyle}
        primary={Boolean(false)}
        fullWidth={this.props.isFullWidth}
        icon={this.props.icon}
        style={this.props.btnStyle}
      />
      <Popover
        useLayerForClickAway={Boolean(false)}
        open={this.state.open}
        anchorEl={this.state.anchorEl}
        anchorOrigin={{horizontal: 'left', vertical: 'top'}}
        targetOrigin={{horizontal: 'left', vertical: 'bottom'}}
        onRequestClose={this.handleRequestClose}
      >
        <Menu autoWidth={Boolean(false)}>
          <div className={styles.childContainer}>
            {this.props.children}
          </div>
        </Menu>
      </Popover>
    </div>
    );
  }
}
