import React, {Component, PropTypes} from 'react';
import PricePopover from './Price/Price';
import CityPopover from './City/City';
import RoomPopover from './Room/Room';
import IdPopover from './Id/Id';
import StreetPopover from './Street/Street';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as searchActions from 'redux/modules/searchForm';
// import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import {Link} from 'react-router';
const styles = require('./SearchForm.scss');

@connect((state) => ({
  startPrice: state.searchForm.priceStart,
  endPrice: state.searchForm.priceEnd,
  predictions: state.searchForm.predictions,
  cityName: state.searchForm.cityName,
  roomStartCount: state.searchForm.roomStartCount,
  roomEndCount: state.searchForm.roomEndCount,
  id: state.searchForm.id,
  street: state.searchForm.street
}), dispatch => bindActionCreators({
  ...searchActions
}, dispatch))
export default class SearchForm extends Component {

  static propTypes = {
    startPrice: PropTypes.node,
    endPrice: PropTypes.node,
    setStartPrice: PropTypes.func,
    setEndPrice: PropTypes.func,
    setCityName: PropTypes.func,
    cityName: PropTypes.node,
    autoComplete: PropTypes.func,
    predictions: PropTypes.array,
    roomStartCount: PropTypes.node,
    roomEndCount: PropTypes.node,
    setRoomStart: PropTypes.func,
    setRoomEnd: PropTypes.func,
    setId: PropTypes.func,
    id: PropTypes.node,
    street: PropTypes.node,
    setStreet: PropTypes.func,
    search: PropTypes.func,
    buttonStyle: PropTypes.object
  }

  static defaultProps = {
    buttonStyle: {
      'marginLeft': '24px',
      'width': '196px',
      'display': 'block',
      'border': '1px solid #4CAF50'
    }
  }

  search() {
    this.props.search({
      startPrice: this.props.startPrice,
      endPrice: this.props.endPrice,
      cityName: this.props.cityName,
      roomStartCount: this.props.roomStartCount,
      roomEndCount: this.props.roomEndCount,
      id: this.props.id,
      street: this.props.street
    });
  }

  render() {
    const {street, setStreet, setId, id, setStartPrice, setEndPrice, startPrice, endPrice, cityName, setCityName, predictions, autoComplete, setRoomStart, setRoomEnd, roomStartCount, roomEndCount, ...other} = this.props;
    return (
      <div className={styles.container}>
        <PricePopover {...other} setStart={setStartPrice} setEnd={setEndPrice} start={startPrice} end={endPrice} />
        <CityPopover {...other} cityName={cityName} setCityName={setCityName} predictions={predictions} autoComplete={autoComplete} />
        <StreetPopover {...other} street={street} setStreet={setStreet} predictions={predictions} autoComplete={autoComplete} />
        <RoomPopover {...other} setStart={setRoomStart} setEnd={setRoomEnd} start={roomStartCount} end={roomEndCount} />
        <IdPopover {...other} setStart={setRoomStart} setId={setId} id={id} />
        <FlatButton style={this.props.buttonStyle} containerElement={<Link style={this.props.buttonStyle} to="/objektai" />} fullWidth={Boolean(false)} onClick={() => this.search()} label="Ieškoti" primary={Boolean(true)} />
      </div>
    );
  }
}
