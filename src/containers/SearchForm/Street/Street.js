import React, {Component, PropTypes} from 'react';
import AutoComplete from 'material-ui/AutoComplete';
import debounce from 'lodash/debounce';
import BasePopover from '../BasePopover/BasePopover';
import Street from 'material-ui/svg-icons/maps/map';

export default class StreetPopover extends Component {

  static propTypes = {
    autoComplete: PropTypes.func,
    predictions: PropTypes.array,
    street: PropTypes.string,
    setStreet: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.autoComplete = debounce(this.autoComplete, 300);
    this.state = {
      isOpen: false
    };
  }

  setQuery(city) {
    return 'input=' + city + '&types=geocode&language=lt';
  }

  getCityName() {
    if (this.props.street === '') {
      return 'Gatvė';
    }
    return this.props.street;
  }

  handleUpdateInput = (value) => {
    this.props.setStreet(value);
    this.autoComplete(value);
  };

  autoComplete(value) {
    this.props.autoComplete(this.setQuery(value));
  }

  render() {
    const iconStyle = {
      'marginTop': '6px',
      'float': 'left',
      'marginLeft': '12px'
    };

    const {setStreet, predictions, street, ...other} = this.props;
    return (
      <BasePopover {...other} icon={<Street style={iconStyle} />} isOpen={this.state.isOpen} name={this.getCityName()}>
        <AutoComplete
          searchText={street}
          onClose={() => this.setState({isOpen: false})}
          onNewRequest={(value) => setStreet(value)}
          hintText="Gatvė"
          dataSource={predictions}
          onUpdateInput={this.handleUpdateInput}
          filter={() => true}/>
      </BasePopover>
    );
  }
}
