import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {Button} from 'react-bootstrap';
import AuthService from '../../utils/AuthService';

export default class Login extends Component {

  static propTypes = {
    location: PropTypes.object,
    auth: PropTypes.instanceOf(AuthService)
  }

  static contextTypes = {
    router: PropTypes.object
  }

  getAuthParams() {
    return {
      email: ReactDOM.findDOMNode(this.refs.email).value,
      password: ReactDOM.findDOMNode(this.refs.password).value
    };
  }

  login(event) {
    event.preventDefault();
    const { email, password } = this.getAuthParams();
    this.props.auth.login(email, password);
  }

  signup() {
    const { email, password } = this.getAuthParams();
    this.props.auth.signup(email, password);
  }

  loginWithGoogle() {
    this.props.auth.loginWithGoogle();
  }

  render() {
    return (
      <div>
        <h2>Prisijungimas</h2>
            <input type="email" ref="email" placeholder="email" required />
            <input type="password" ref="password" placeholder="Slaptažodis" required />
            <Button onClick={this.login.bind(this)} type="submit" bsStyle="primary">Prisijungti</Button>
      </div>
    );
  }
}
