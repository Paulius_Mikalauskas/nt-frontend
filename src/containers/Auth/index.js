import React, {Component, PropTypes} from 'react';
import {Register, Login} from 'components';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
const styles = require('./index.scss');
import {Tabs, Tab} from 'material-ui/Tabs';
import {login} from 'redux/modules/login';
import {register} from 'redux/modules/register';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

@connect(() => ({}), dispatch => bindActionCreators({
  login: login,
  register: register
}, dispatch))
export default class Auth extends Component {

  static propTypes = {
    login: PropTypes.func,
    register: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      email: '',
      password: '',
      password2: '',
      currentTab: 0
    };
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handlePassword2Change = this.handlePassword2Change.bind(this);
  }

  submitBtn() {
    if (this.state.currentTab === 0) {
      return (
        <FlatButton
          label = "Prisijungti"
          primary = {Boolean(true)}
          keyboardFocused = {Boolean(true)}
          onTouchTap = {this.handleClose}
          onClick={() => this.props.login({
            email: this.state.email,
            password: this.state.password
          })}
        />
      );
    }
    return (
      <FlatButton
        label = "Registruotis"
        primary = {Boolean(true)}
        keyboardFocused = {Boolean(true)}
        onTouchTap = {this.handleClose}
        disabled={this.isFormValid()}
        onClick={() => this.props.register({
          email: this.state.email,
          password: this.state.password
        })}
      />
    );
  }

  isFormValid() {
    return this.state.password !== this.state.password2;
  }

  tabStyle() {
    return {
      'backgroundColor': 'white',
      'color': '#333',
      'border': '1px solid rgb(200, 230, 201)'};
  }

  bodyStyle() {
    return {'maxWidth': '500px'};
  }

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleEmailChange(event) {
    this.setState({email: event.target.value});
  }

  handlePasswordChange(event) {
    this.setState({password: event.target.value});
  }

  handlePassword2Change(event) {
    this.setState({password2: event.target.value});
  }

  handleTabSwitch(value) {
    this.setState({
      currentTab: value,
      email: '',
      password: '',
      password2: ''
    });
  }

  render() {
    const actions = [
      <FlatButton
        label = "Atšaukti"
        primary = {Boolean(true)}
        onTouchTap = {this.handleClose}
      />,
      this.submitBtn()
    ];

    return (
      <div>
        <FlatButton
          className={styles.btnStyle}
          label="Login"
          onTouchTap={this.handleOpen}
        />
        <Dialog
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          contentStyle={this.bodyStyle()}
          >
          <Tabs
            onChange={(eve) => this.handleTabSwitch(eve)}
            >
            <Tab style={this.tabStyle()} label="Prisijungimas" value={0}>
              <Login
                handleEmailChange={this.handleEmailChange}
                handlePasswordChange={this.handlePasswordChange}
                email={this.state.email}
                password={this.state.password}
              />
            </Tab>
            <Tab style={this.tabStyle()} label="Registracija" value={1}>
              <Register
                handleEmailChange={this.handleEmailChange}
                handlePasswordChange={this.handlePasswordChange}
                handlePassword2Change={this.handlePassword2Change}
                email={this.state.email}
                password={this.state.password}
                password2={this.state.password2}
                isFormValid={this.isFormValid()}
              />
            </Tab>
          </Tabs>
        </Dialog>
      </div>
    );
  }
}
