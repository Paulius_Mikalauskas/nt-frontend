import React, {Component, PropTypes} from 'react';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import Menu from 'material-ui/svg-icons/navigation/menu';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import SideMenu from '../../components/SideMenu/SideMenu';
import {Grid} from 'react-bootstrap';
import injectTapEventPlugin from 'react-tap-event-plugin';
import {RouteTransition} from 'react-router-transition';
import {setStatus} from 'redux/modules/login';
import AppBar from 'material-ui/AppBar';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import {Auth} from 'containers';

injectTapEventPlugin();

const Logged = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem primaryText="Refresh" />
    <MenuItem primaryText="Help" />
    <MenuItem primaryText="Sign out" />
  </IconMenu>
);

@connect(() => ({}), dispatch => bindActionCreators({
  setStatus: setStatus
}, dispatch))
export default class App extends Component {

  static propTypes = {
    children: PropTypes.object,
    toggle: PropTypes.func,
    isToggled: PropTypes.bool,
    location: PropTypes.object,
    route: PropTypes.object,
    setStatus: PropTypes.func,
    isLoggedIn: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state = {
      isToggled: true
    };
  }

  componentDidMount() {
    this.props.setStatus(this.props.route.auth.loggedIn());
  }

  toggle() {
    const nextState = !this.state.isToggled;
    this.setState({isToggled: nextState});
  }

  render() {
    let children = null;
    if (this.props.children) {
      children = React.cloneElement(this.props.children, {
        auth: this.props.route.auth
      });
    }

    const styles = require('./App.scss');
    return (
      <div>
        <AppBar
          className={styles.appBar}
          title="NT portalas"
          iconElementLeft={<IconButton><Menu onClick={() => this.toggle()} /></IconButton>}
          iconElementRight={this.props.isLoggedIn ? <Logged /> : <Auth />}
        />
        <Grid fluid>
          <div className={styles.wrapper + ' ' + (this.state.isToggled
            ? styles.sideMenuWrapperOpen
            : styles.sideMenuWrapperClosed)}>
            <SideMenu isToggled={this.state.isToggled} setStatus={this.props.setStatus} logout={this.props.route.auth.logout}/>
            <div className={styles.margin}>
              <RouteTransition pathname={this.props.location.pathname} atEnter={{
                transition: 0.15,
                bottom: 20,
                opacity: 0.8
              }} atLeave={{}} atActive={{
                transition: 0.15,
                bottom: 0,
                opacity: 1
              }}>
                {children}
              </RouteTransition>
            </div>
          </div>
        </Grid>
      </div>
    );
  }
}

SideMenu.defaultProps = {
  isToggled: true
};
