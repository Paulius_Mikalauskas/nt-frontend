import cookie from 'react-cookie';

const UPDATE_STARTED = 'update/UPDATE_STARTED';
const UPDATE_FAILED = 'update/UPDATE_FAILED';
const UPDATE_SUCCESS = 'update/UPDATE_SUCCESS';
const REMOVE_SUCCESS = 'update/REMOVE_SUCCESS';
const REMOVE_STARTED = 'update/REMOVE_STARTED';
const REMOVE_FAILED = 'update/REMOVE_FAILED';
const IMAGE_ADDED = 'update/IMAGE_ADDED';

const initialState = {
  isUpdateFailed: false,
  isUpdateLoading: false,
  isRemoveFailed: false,
  isRemoveLoading: false,
  message: '',
  removeMsg: '',
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case UPDATE_STARTED:
      return {
        ...state,
        isUpdateLoading: true,
        isUpdateFailed: false,
        message: ''
      };
    case UPDATE_FAILED:
      return {
        ...state,
        isUpdateLoading: false,
        isUpdateFailed: true,
        message: 'Įvyko klaida'
      };
    case UPDATE_SUCCESS:
      console.log(action);
      return {
        ...state,
        isUpdateLoading: false,
        isUpdateFailed: false,
        message: 'Objektas sėkmingai atnaujintas'
      };
    case REMOVE_SUCCESS:
      return {
        ...state,
        isRemoveFailed: false,
        isRemoveLoading: false,
        removeMsg: 'Objektas sėkmingai ištrintas'
      };
    case REMOVE_STARTED:
      return {
        ...state,
        isRemoveFailed: false,
        isRemoveLoading: true,
        removeMsg: ''
      };
    case REMOVE_FAILED:
      return {
        ...state,
        isRemoveFailed: true,
        isRemoveLoading: false,
        removeMsg: 'Įvyko klaida'
      };
    case IMAGE_ADDED:
      console.log(action);
      return {
        ...state,
        isUpdateLoading: false,
        isUpdateFailed: false,
        message: 'Objektas sėkmingai atnaujintas',
      };
    default:
      return state;
  }
}

export function update(data, id) {
  return {
    types: [
      UPDATE_STARTED, UPDATE_SUCCESS, UPDATE_FAILED
    ],
    promise: (client) => client.post('/api/update/' + id, {
      params: data,
      headers: {
        'authorization': cookie.load('id_token')
      }
    })
  };
}

export function addImage(image, id, cb) {
  const files = [{key: 'files[]', value: image}];
  return {
    types: [
      UPDATE_STARTED, IMAGE_ADDED, UPDATE_FAILED
    ],
    promise: (client) => client.post('/api/addImage/' + id, {
      attach: files,
    }).then((data) => cb(data))
  };
}

export function removeImage(id, cb) {
  return {
    types: [
      UPDATE_STARTED, IMAGE_ADDED, UPDATE_FAILED
    ],
    promise: (client) => client.post('/api/removeImage/' + id, {}).then(() => cb())
  };
}

export function removeAdvert(id) {
  return {
    types: [
      REMOVE_STARTED, REMOVE_SUCCESS, REMOVE_FAILED
    ],
    promise: (client) => client.post('/api/remove/' + id, {
      headers: {
        'authorization': cookie.load('id_token')
      }
    })
  };
}
