import cookie from 'react-cookie';

const UPLOAD_STARTED = 'upload/UPLOAD_STARTED';
const UPLOAD_FAILED = 'upload/UPLOAD_FAILED';
const UPLOAD_SUCCESS = 'upload/UPLOAD_SUCCESS';

const initialState = {
  isFailed: false,
  isLoading: false,
  message: ''
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case UPLOAD_STARTED:
      return {
        ...state,
        isLoading: true,
        isFailed: false,
        message: ''
      };
    case UPLOAD_FAILED:
      console.log(action.result);
      return {
        ...state,
        isLoading: false,
        isFailed: true,
        message: 'Įvyko klaida'
      };
    case UPLOAD_SUCCESS:
      console.log(action.result);
      return {
        ...state,
        isLoading: false,
        isFailed: false,
        message: 'Objektas sėkmingai įkeltas'
      };
    default:
      return state;
  }
}

export function upload(images, data) {
  const files = images.map((item) => {
    return {key: 'files[]', value: item};
  });
  return {
    types: [
      UPLOAD_STARTED, UPLOAD_SUCCESS, UPLOAD_FAILED
    ],
    promise: (client) => client.post('/api/upload', {
      headers: {'Authorization': 'Bearer ' + cookie.load('id_token')},
      attach: files,
      params: {...data, auth: cookie.load('id_token')},
    })
  };
}

export function test(images) {
  const files = [];
  images.map((item) => {
    files.push({key: 'files[]', value: item});
  });
  console.log(files);
  return {
    types: [
      UPLOAD_STARTED, UPLOAD_SUCCESS, UPLOAD_FAILED
    ],
    promise: (client) => client.post('/api/test', {
      attach: files,
    })
  };
}
