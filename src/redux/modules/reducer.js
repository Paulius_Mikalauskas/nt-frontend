import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import {reducer as reduxAsyncConnect} from 'redux-async-connect';
import { pagination } from 'violet-paginator';
import advertisement from './advertisement';
import objectDetails from './objectDetails';
import imageViewer from './imageViewer';
import uploadReducer from './upload';
import login from './login';
import register from './register';
import objectForm from './form';
import updateReducer from './update';
import searchForm from './searchForm';
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
  routing: routerReducer,
  reduxAsyncConnect,
  advertisement,
  objectDetails,
  imageViewer,
  pagination,
  uploadReducer,
  login,
  register,
  objectForm,
  updateReducer,
  searchForm,
  form: formReducer
});
