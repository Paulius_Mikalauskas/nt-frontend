const FETCH_STARTED = 'advertisement/FETCH_STARTED';
const FETCH_FAILED = 'advertisement/FETCH_FAILED';
const FETCH_SUCCESS = 'advertisement/FETCH_SUCCESS';

const initialState = {
  isFetching: false,
  isFailed: false,
  isFetched: false,
  ads: [{
    images: [{url: '', thumbnail: ''}, {url: '', thumbnail: ''}],
    type: '',
    city: '',
    catchment: '',
    street: '',
    area: '',
    stories: '',
    rooms: '',
    price: '',
    description: ''
  }]
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_STARTED:
      return {
        ...state,
        isFetching: true,
        isFailed: false
      };
    case FETCH_FAILED:
      return {
        ...state,
        isFetching: false,
        isFailed: true
      };
    case FETCH_SUCCESS:
      return {
        ...state,
        ads: action.result,
        isFetched: true,
        isFailed: false
      };
    default:
      return state;
  }
}

export function fetch() {
  return {
    types: [FETCH_STARTED, FETCH_SUCCESS, FETCH_FAILED],
    promise: (client) => client.get('/api/objects')
  };
}

export function fetchById(id) {
  return {
    types: [FETCH_STARTED, FETCH_SUCCESS, FETCH_FAILED],
    promise: (client) => client.get('/api/objects/' + id)
  };
}

export function isFetched(globalState) {
  return globalState.advertisements && globalState.advertisements.isFetched;
}
