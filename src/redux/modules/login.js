const SET_STATUS = 'auth/SET_STATUS';
const LOGIN_STARTED = 'auth/LOGIN_STARTED';
const LOGIN_FAILED = 'auth/LOGIN_FAILED';
const LOGIN_SUCCESS = 'auth/LOGIN_SUCCESS';
const LOGOUT = 'auth/LOGOUT';
import cookie from 'react-cookie';

const initialState = {
  isLoggedIn: false,
  loading: false,
  error: false,
  success: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_STATUS:
      return {
        ...state,
        isLoggedIn: action.status
      };
    case LOGIN_STARTED:
      return {
        ...state,
        loading: true,
        error: false,
        success: false
      };
    case LOGIN_FAILED:
      return {
        ...state,
        loading: false,
        error: true,
        success: false
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        loading: false,
        error: false,
        success: true
      };
    case LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
      };
    default:
      return state;
  }
}

export function setStatus(status) {
  return {type: SET_STATUS, status: status};
}

export function login(user) {
  console.log(user);
  return {
    types: [LOGIN_STARTED, LOGIN_SUCCESS, LOGIN_FAILED],
    promise: (client) => client.post('/api/login/', {
      params: user
    })
    .then((token) => {
      cookie.save('id_token', token, {path: '/'});
    })
  };
}
