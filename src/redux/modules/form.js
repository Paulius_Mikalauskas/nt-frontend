const REMOVE_FIELD = 'form/REMOVE_FIELD';
const ADD_IMAGE = 'form/ADD_IMAGE';
const REMOVE_IMAGE = 'form/REMOVE_IMAGE';
const RESTART_FORM = 'form/RESTART_FORM';
const SET_FORM_DETAILS_BY_ID = 'form/SET_FORM_DETAILS_BY_ID';

const initialState = {
  saleState: '',
  objectType: '',
  formFields: {},
  selectedImages: [],
  data: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case RESTART_FORM:
      return {
        ...state, ...initialState
      };
    case SET_FORM_DETAILS_BY_ID:
      return {
        ...state,
        formFields: action.data,
        selectedImages: action.data.images.map((item) => {
          return item;
        })
      };
    case ADD_IMAGE:
      return {
        ...state,
        selectedImages: [
          ...state.selectedImages, {
            id: state.selectedImages.length !== 0
              ? state.selectedImages[state.selectedImages.length - 1].id + 1
              : 1,
            data: action.image
          }
        ]
      };
    case REMOVE_IMAGE:
      return {
        ...state,
        selectedImages: state.selectedImages.filter((item) => {
          return item.id !== action.id;
        })
      };
    case REMOVE_FIELD:
      return {
        ...state,
        formFields: state.formFields.filter((item) => {
          return item.id !== action.id;
        })
      };
    default:
      return state;
  }
}

export function addImage(image) {
  return {type: ADD_IMAGE, image: image};
}

export function removeImage(id) {
  return {type: REMOVE_IMAGE, id: id};
}

export function restartForm() {
  return {type: RESTART_FORM};
}

export function setEditData(data) {
  return {type: SET_FORM_DETAILS_BY_ID, data: data};
}
