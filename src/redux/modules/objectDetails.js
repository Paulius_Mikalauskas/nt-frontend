const FETCH_STARTED = 'objectDetails/FETCH_STARTED';
const FETCH_FAILED = 'objectDetails/FETCH_FAILED';
const FETCH_SUCCESS = 'objectDetails/FETCH_SUCCESS';
const SET_DETAILS = 'objectDetails/SET_DETAILS';

const SET_FORM_DETAILS = 'form/SET_DETAILS';


const initialState = {
  isFetching: false,
  isFailed: false,
  details: {
    images: [{url: ''}, {url: ''}],
    type: '',
    city: '',
    catchment: '',
    street: '',
    area: '',
    stories: '',
    rooms: '',
    price: '',
    description: ''
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case FETCH_STARTED:
      return {
        ...state,
        isFetching: true,
        isFailed: false
      };
    case FETCH_FAILED:
      return {
        ...state,
        isFetching: false,
        isFailed: true
      };
    case FETCH_SUCCESS:
      console.log(state);
      console.log(action);
      console.log(action.result);
      return {
        ...state,
        details: action.result,
        isFetched: true,
        isFailed: false
      };
    case SET_DETAILS:
      return {
        ...state,
        details: action.details
      };
    default:
      return state;
  }
}

export function fetch(id) {
  return {
    types: [
      FETCH_STARTED, FETCH_SUCCESS, FETCH_FAILED
    ],
    promise: (client) => client.get('/api/details/' + id)
  };
}

export function fetchForEditing(id) {
  return {
    types: [
      FETCH_STARTED, SET_FORM_DETAILS, FETCH_FAILED
    ],
    promise: (client) => client.get('/api/details/' + id)
  };
}
