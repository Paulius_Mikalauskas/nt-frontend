const SET_START_PRICE = 'searchForm/SET_START_PRICE';
const SET_END_PRICE = 'searchForm/SET_END_PRICE';
const SET_CITY_NAME = 'searchForm/SET_CITY_NAME';
const SET_ROOM_END_COUNT = 'searchForm/SET_ROOM_END_COUNT';
const SET_ROOM_START_COUNT = 'searchForm/SET_ROOM_START_COUNT';
const AUTOCOMPLETE_SUCESS = 'searchForm/AUTOCOMPLETE_SUCESS';
const AUTOCOMPLETE_STARTED = 'searchForm/AUTOCOMPLETE_STARTED';
const AUTOCOMPLETE_FAILED = 'searchForm/AUTOCOMPLETE_FAILED';
const SET_ID = 'searchForm/SET_ID';
const SET_STREET = 'searchForm/SET_STREET';
const SEARCH_STARTED = 'searchForm/SEARCH_STARTED';
const SEARCH_SUCCESS = 'searchForm/SEARCH_SUCCESS';
const SEARCH_FAILED = 'searchForm/SEARCH_FAILED';

const initialState = {
  priceStart: '',
  priceEnd: '',
  city: '',
  predictions: [],
  cityName: '',
  roomStartCount: '',
  roomEndCount: '',
  'id': '',
  'street': '',
  results: []
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_START_PRICE:
      return {
        ...state,
        priceStart: action.value
      };
    case SET_END_PRICE:
      return {
        ...state,
        priceEnd: action.value
      };
    case SET_CITY_NAME:
      return {
        ...state,
        cityName: action.value
      };
    case SET_ROOM_START_COUNT:
      return {
        ...state,
        roomStartCount: action.value
      };
    case SET_ROOM_END_COUNT:
      return {
        ...state,
        roomEndCount: action.value
      };
    case SET_ID:
      return {
        ...state,
        id: action.value
      };
    case SET_STREET:
      return {
        ...state,
        street: action.value
      };
    case AUTOCOMPLETE_SUCESS:
      return {
        ...state,
        predictions: action.result.predictions.map((item, index) => {
          if (index < 3) {
            return item.structured_formatting.main_text;
          }
        })
      };
    case AUTOCOMPLETE_STARTED:
      return {
        ...state,
        predictions: []
      };
    case AUTOCOMPLETE_FAILED:
      return state;
    case SEARCH_STARTED:
      return state;
    case SEARCH_SUCCESS:
      return {
        ...state,
        results: action.result
      };
    case SEARCH_FAILED:
      return state;
    default:
      return state;
  }
  return state;
}

export function setStartPrice(value) {
  return {type: SET_START_PRICE, value: value};
}

export function setEndPrice(value) {
  return {type: SET_END_PRICE, value: value};
}

export function setCityName(value) {
  return {type: SET_CITY_NAME, value: value};
}

export function setRoomStart(value) {
  return {type: SET_ROOM_START_COUNT, value: value};
}

export function setRoomEnd(value) {
  return {type: SET_ROOM_END_COUNT, value: value};
}

export function setId(value) {
  return {type: SET_ID, value: value};
}

export function setStreet(value) {
  return {type: SET_STREET, value: value};
}

export function search(data) {
  return {
    types: [
      SEARCH_STARTED, SEARCH_SUCCESS, SEARCH_FAILED
    ],
    promise: (client) => client.post('/api/search', {params: data})
  };
}

export function autoComplete(query) {
  return {
    types: [
      AUTOCOMPLETE_STARTED, AUTOCOMPLETE_SUCESS, AUTOCOMPLETE_FAILED
    ],
    promise: (client) => client.get('/api/autocomplete/' + query)
  };
}
