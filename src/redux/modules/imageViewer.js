const INCREMENT = 'imageViewer/INCREMENT';
const DECREMENT = 'imageViewer/DECREMENT';
const SET_INDEX = 'imageViewer/SET_INDEX';
const OPEN = 'imageViewer/OPEN';
const CLOSE = 'imageViewer/CLOSE';

const initialState = {
  index: 0,
  isOpen: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case INCREMENT:
      return {
        ...state,
        index: state.index + 1
      };
    case DECREMENT:
      return {
        ...state,
        index: state.index - 1
      };
    case SET_INDEX:
      console.log(action.index);
      return {
        ...state,
        index: action.index
      };
    case CLOSE:
      return {
        ...state, isOpen: false
      };
    case OPEN:
      return {
        ...state, isOpen: true
      };
    default:
      return state;
  }
}

export function increment() {
  return {type: INCREMENT};
}

export function decrement() {
  return {type: DECREMENT};
}

export function setIndex(index) {
  return {type: SET_INDEX, index: index};
}

export function open() {
  return {type: OPEN};
}

export function close() {
  return {type: CLOSE};
}
