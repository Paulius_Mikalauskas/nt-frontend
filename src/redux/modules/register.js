const REGISTER_STARTED = 'auth/REGISTER_STARTED';
const REGISTER_FAILED = 'auth/REGISTER_FAILED';
const REGISTER_SUCCESS = 'auth/REGISTER_SUCCESS';

const initialState = {
  loading: false,
  error: false,
  success: false
};
export default function reducer(state = initialState, action) {
  switch (action.type) {
    case REGISTER_STARTED:
      return {
        ...state,
        loading: false,
        error: false,
        success: true
      };
    case REGISTER_FAILED:
      return {
        ...state,
        loading: false,
        error: false,
        success: true
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        success: true
      };
    default:
      return state;
  }
}

export function register(user) {
  return {
    types: [
      REGISTER_STARTED, REGISTER_SUCCESS, REGISTER_FAILED
    ],
    promise: (client) => client.post('/api/register/', {
      params: user
    })
  };
}
