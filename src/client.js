/**
 * THIS IS THE ENTRY POINT FOR THE CLIENT, JUST LIKE server.js IS THE ENTRY POINT FOR THE SERVER.
 */
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import createStore from './redux/create';
import ApiClient from './helpers/ApiClient';
import {Provider} from 'react-redux';
import {Router, browserHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';
import {ReduxAsyncConnect} from 'redux-async-connect';
import useScroll from 'scroll-behavior/lib/useStandardScroll';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import getRoutes from './routes';

const client = new ApiClient();
const _browserHistory = useScroll(() => browserHistory)();
const dest = document.getElementById('content');
const store = createStore(_browserHistory, client, window.__data);
const history = syncHistoryWithStore(_browserHistory, store);
import {green100, green500, green700} from 'material-ui/styles/colors';

const muiTheme = getMuiTheme({
  fontFamily: 'Roboto, Arial, sans-serif',
  palette: {
    primary1Color: green500,
    primary2Color: green700,
    primary3Color: green100
  },
  svgIcon: {
    color: '#FFFFFF'
  },
  listItem: {
    height: 40
  }
}, {
  avatar: {
    borderColor: null
  }
});

const component = (
  <Router render={(props) => <ReduxAsyncConnect {...props} helpers={{
    client
  }} filter={item => !item.deferred}/>} history={history}>
    {getRoutes(store)}
  </Router>
);

ReactDOM.render(
  <MuiThemeProvider muiTheme={muiTheme}>
  <Provider store={store} key="provider">
    {component}
  </Provider>
</MuiThemeProvider>, dest);

if (process.env.NODE_ENV !== 'production') {
  window.React = React; // enable debugger

  if (!dest || !dest.firstChild || !dest.firstChild.attributes || !dest.firstChild.attributes['data-react-checksum']) {
    console.error('Server-side React render was discarded. Make sure that your initial render does not contain any client-side code.');
  }
}

if (__DEVTOOLS__ && !window.devToolsExtension) {
  const DevTools = require('./containers/DevTools/DevTools');
  ReactDOM.render(
    <MuiThemeProvider muiTheme={muiTheme}>
    <Provider store={store} key="provider">
      <div>
        {component}
        <DevTools/>
      </div>
    </Provider>
  </MuiThemeProvider>, dest);
}
